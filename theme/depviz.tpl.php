<?php

/**
 * @file
 * Theme the dependency visualization page.
 *
 * - $categories: Module categories.
 */
?>
<div class="depviz-wrapper">
  <div id="depviz-graph"></div>
  <div class="depviz-loadingBar">
    <div class="depviz-outerBorder">
      <div class="depviz-loadingText">0%</div>
      <div class="depviz-loadingBoarder">
        <div class="depviz-bar"></div>
      </div>
    </div>
  </div>
</div>
<div class="info-box"><?php print $categories ?></div>
<div>
  <form>
    <input type="text" name="depviz-search-text" id="depviz-search-text"
           class="form-text"
           placeholder="Search module">
  </form>
</div>
