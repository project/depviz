/**
 * @file
 * Depviz module init js.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.cm_agenda_editor = {
    attach: function (context, settings) {
      // Create an array with nodes.
      var nodes = new vis.DataSet(Drupal.settings.depviz.nodes);

      // Create an array with edges.
      var edges = new vis.DataSet(Drupal.settings.depviz.edges);

      // Create a network.
      var container = document.getElementById('depviz-graph');
      var data = {
        nodes: nodes,
        edges: edges
      };
      var options = {
        layout: {improvedLayout: false},
        physics: {enabled: true, solver: 'forceAtlas2Based', timestep: 0.1}
      };
      Drupal.depviz = {};
      Drupal.depviz.network = new vis.Network(container, data, options);
      Drupal.depviz.network.on('stabilizationProgress', function (params) {
        var maxWidth = 496;
        var minWidth = 20;
        var widthFactor = params.iterations / params.total;
        var width = Math.max(minWidth, maxWidth * widthFactor);
        $('.depviz-loadingBar .depviz-bar').width(width + 'px');
        $('.depviz-loadingText').html(Math.round(widthFactor * 100) + '%');
      });
      Drupal.depviz.network.once('stabilizationIterationsDone', function () {
        $('.depviz-loadingText').html('100%');
        $('.depviz-loadingBar .depviz-bar').width('496px');
        $('.depviz-loadingBar').fadeTo('slow', 0.33);
        // Clean the DOM element.
        setTimeout(function () {
          $('.depviz-loadingBar').hide();
        }, 500);
      });
      Drupal.depviz.autocomplete = $('#depviz-search-text').autocomplete({
        source: Drupal.settings.depviz.nodes,
        select: function (event, ui) {
          Drupal.depviz.network.focus(ui.item.id, {scale: 1, animation: true});
        }
      });
    }
  };
})(jQuery, Drupal);
