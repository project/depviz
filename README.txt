Drupal Exit Modal module:
-------------------------
Maintainer:
  Kristoffer Wiklund (https://www.drupal.org/u/kristofferwiklund)
  Requires - Drupal 7 and Vis.js
  License - GPL (see LICENSE)

Overview:
---------
Simple module to visually display active modules dependencies. It uses the
Vis.js module (that uses the Vis.js library) to display the graph. The graph is
interactive and mobile friendly.

Installation:
-------------
1. Download and unpack the Dependency visualization module directory in your
   modules folder (this will usually be "sites/all/modules/").
2. Go to "Administer" -> "Modules" and enable the Dependency visualization
   module.


Configuration:
--------------
Dependency visualization has no configuration. Dependency visualization graph
can be viewed at "Administer" -> "Modules" -> "Dependency visualization".
